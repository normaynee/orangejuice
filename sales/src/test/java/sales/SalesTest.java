package sales;

import org.demo.basic.bean.CalculateOrder;
import org.junit.Assert;
import org.junit.Test;

public class SalesTest {
	
	@Test
	public void CalcOrder() {
		final Float expected = 80f;
		
		final Float actual = (40 *2) + 0f;
		
		Assert.assertEquals(actual, expected);
	}
}
