package org.demo.basic.bean;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.Arrays;
import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.text.JTextComponent;

import java.awt.SystemColor;
import javax.swing.JSeparator;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JPasswordField;

import java.awt.Toolkit;
import java.awt.Scrollbar;

public class GUI_Sales{


	private JFrame frame;
	private static JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private static JTextField textField_5;
	private JTextField textField_6;
	private JPasswordField passwordField;
	public static JLabel lblNewLabel_8;
	public static JTabbedPane comboBox;
	private static JTextField textField;
	private static JTextField textField_7;
	private static JTextField textField_8;
	public static JTextComponent lblNewLabel_9;
	public static JTextComponent textPane;
	public static JTextComponent textArea_1;
	final Runnable runnable =
	(Runnable) Toolkit.getDefaultToolkit().getDesktopProperty("win.sound.exclamation");
		 
	public static String text;
	static Snack snack = new Snack();
	static Student student = new Student();
	static Order order = new Order();
	static orderFile orderf = new orderFile();
	static CalculateOrder cal = new CalculateOrder();
	static readOrders ord = new readOrders();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI_Sales window = new GUI_Sales();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI_Sales() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 620, 508);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 585, 437);
		frame.getContentPane().add(tabbedPane);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Main", null, panel_2, null);
		panel_2.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 580, 221);
		panel_2.add(panel);
		panel.setBackground(UIManager.getColor("Table.light"));
		panel.setLayout(null);
		
		JLabel lblGeneral = new JLabel("Snacks");
		lblGeneral.setBounds(20, 11, 56, 16);
		panel.add(lblGeneral);

		JList<?> list = new JList<Object>();
		list.setBounds(65, 104, 42, -40);
		panel.add(list);
		
		JLabel lblSnack = new JLabel("Snack");
		lblSnack.setBounds(20, 50, 46, 14);
		panel.add(lblSnack);
		
		JLabel lblNewLabel = new JLabel("Quantity");
		lblNewLabel.setBounds(20, 134, 56, 14);
		panel.add(lblNewLabel);
		
		textField_1 = new JTextField();
		textField_1.setBounds(76, 131, 63, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		 lblNewLabel_8 = new JLabel("");
		lblNewLabel_8.setBackground(Color.WHITE);
		lblNewLabel_8.setBounds(236, 50, 134, 98);
		panel.add(lblNewLabel_8);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(20, 25, 400, 8);
		panel.add(separator);
		
		final JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
				
				text = (String)comboBox.getSelectedItem();	

				if (text == "Banana Chips")
				{
					textField.setText("BAN10");
					textField_7.setText("70");
					Image img = new ImageIcon(this.getClass().getResource("/banana chips.jpg")).getImage();
					lblNewLabel_8.setIcon(new ImageIcon(img)); 
				}
				if (text == "Potato Chips")
				{
					textField.setText("POTC");
					textField_7.setText("70");
					Image img = new ImageIcon(this.getClass().getResource("/potato chips.jpg")).getImage();
					lblNewLabel_8.setIcon(new ImageIcon(img)); 
				}
				if (text == "Plaintain Chips")
				{
					textField.setText("PLTN");
					textField_7.setText("90");
					Image img = new ImageIcon(this.getClass().getResource("/plantain chips.jpg")).getImage();
					lblNewLabel_8.setIcon(new ImageIcon(img)); 
				}
				if (text == "Peanuts")
				{
					textField.setText("NUTS");
					textField_7.setText("60");
					Image img = new ImageIcon(this.getClass().getResource("/peanuts.jpg")).getImage();
					lblNewLabel_8.setIcon(new ImageIcon(img)); 
				}
				if (text == "Cashews")
				{
					textField.setText("CASH");
					textField_7.setText("150");
					Image img = new ImageIcon(this.getClass().getResource("/cashew.jpg")).getImage();
					lblNewLabel_8.setIcon(new ImageIcon(img)); 
				}
				if (text == "Bun & Cheese")
				{
					textField.setText("BNCH");
					textField_7.setText("170");
					Image img = new ImageIcon(this.getClass().getResource("/bun.jpg")).getImage();
					lblNewLabel_8.setIcon(new ImageIcon(img)); 
				}
				if (text == "Oreos")
				{
					textField.setText("OREO");
					textField_7.setText("130");
					Image img = new ImageIcon(this.getClass().getResource("/oreo.jpg")).getImage();
					lblNewLabel_8.setIcon(new ImageIcon(img)); 
				}
				if (text == "Ovaltine")
				{
					textField.setText("OVAL");
					textField_7.setText("40");
					Image img = new ImageIcon(this.getClass().getResource("/ovaltine.jpg")).getImage();
					lblNewLabel_8.setIcon(new ImageIcon(img)); 
				}
				if (text == "Duplex")
				{
					textField.setText("DPLX");
					textField_7.setText("40");
					Image img = new ImageIcon(this.getClass().getResource("/duplex.jpg")).getImage();
					lblNewLabel_8.setIcon(new ImageIcon(img)); 
				}
				if (text == "Cup Soup")
				{
					textField.setText("CUP12");
					textField_7.setText("100");
					Image img = new ImageIcon(this.getClass().getResource("/cup soup.jpg")).getImage();
					lblNewLabel_8.setIcon(new ImageIcon(img)); 
				}
			}
		});
		comboBox.setBounds(76, 44, 114, 20);
		comboBox.addItem("---------------------");
		comboBox.addItem("Banana Chips");
		comboBox.addItem("Potato Chips");
		comboBox.addItem("Plaintain Chips");
		comboBox.addItem("Peanuts");
		comboBox.addItem("Cashews");
		comboBox.addItem("Bun & Cheese");
		comboBox.addItem("Oreos");
		comboBox.addItem("Ovaltine");
		comboBox.addItem("Duplex");
		comboBox.addItem("Cup Soup");
		panel.add(comboBox);
		
		JLabel lblNewLabel_6 = new JLabel("ID");
		lblNewLabel_6.setBounds(20, 109, 46, 14);
		panel.add(lblNewLabel_6);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(76, 104, 63, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_7 = new JLabel("Price");
		lblNewLabel_7.setBounds(20, 75, 46, 14);
		panel.add(lblNewLabel_7);
		
		textField_7 = new JTextField();
		textField_7.setEditable(false);
		textField_7.setBounds(75, 73, 63, 20);
		panel.add(textField_7);
		textField_7.setColumns(10);
		
		JButton btnNewButton = new JButton("ADD");
		panel.add(btnNewButton);
		btnNewButton.setBounds(76, 183, 97, 25);
		btnNewButton.addActionListener(new Action ());
		
		textField_8 = new JTextField();
		textField_8.setEditable(false);
		textField_8.setText("0");
		textField_8.setBounds(397, 184, 171, 22);
		panel.add(textField_8);
		textField_8.setColumns(10);
		
		JLabel lblTotal = new JLabel("Total");
		lblTotal.setBounds(339, 187, 56, 16);
		panel.add(lblTotal);
		
		textArea_1 = new JTextArea();
		textArea_1.setEditable(false);
		textArea_1.setBounds(397, 46, 171, 125);
		panel.add(textArea_1);
		
		Scrollbar scrollbar = new Scrollbar();
		scrollbar.setBounds(551, 50, 17, 121);
		panel.add(scrollbar);
		
				

		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 234, 580, 173);
		panel_2.add(panel_1);
		panel_1.setBackground(SystemColor.controlHighlight);
		panel_1.setLayout(null);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(28, 24, 390, 14);
		panel_1.add(separator_1);
		
		JLabel lblNewLabel_1 = new JLabel("Student");
		lblNewLabel_1.setBounds(28, 11, 46, 14);
		panel_1.add(lblNewLabel_1);
		
		textField_2 = new JTextField();
		textField_2.setBounds(110, 39, 103, 20);
		panel_1.add(textField_2);
		textField_2.setColumns(10);
		
		
		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setBounds(28, 42, 72, 14);
		panel_1.add(lblFirstName);
		
		textField_3 = new JTextField();
		textField_3.setBounds(315, 39, 103, 20);
		panel_1.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(241, 42, 78, 14);
		panel_1.add(lblLastName);
		
		JLabel lblLocation = new JLabel("Location");
		lblLocation.setBounds(28, 101, 72, 14);
		panel_1.add(lblLocation);
		
		textField_4 = new JTextField();
		textField_4.setBounds(110, 98, 209, 20);
		panel_1.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("ID ");
		lblNewLabel_2.setBounds(28, 70, 22, 14);
		panel_1.add(lblNewLabel_2);
		
		textField_5 = new JTextField();
		textField_5.setBounds(110, 67, 86, 20);
		panel_1.add(textField_5);
		textField_5.setColumns(10);
		
		JButton btnNewButton_1 = new JButton("SUBMIT");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String fName = textField_2.getText();
				String LName = textField_3.getText();
				String id = textField_5.getText();
				String location = textField_4.getText();
				String data = textArea_1.getText();
				
				orderf.addStudent(id, fName, LName, location, data);
				textField_1.setText("");
				textField_5.setText(" ");
				textField_7.setText("");
				textField_2.setText("");
				textField_3.setText(" ");
				textField_4.setText("");
				textField_8.setText("0");
				textArea_1.setText("");

				
			}
		});
		
		btnNewButton_1.setBounds(264, 137, 89, 23);
		panel_1.add(btnNewButton_1);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Login", null, panel_3, null);
		panel_3.setLayout(null);
		
		JLabel lblNewLabel_3 = new JLabel("Administrator");
		lblNewLabel_3.setBounds(24, 11, 103, 14);
		panel_3.add(lblNewLabel_3);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(20, 26, 406, 14);
		panel_3.add(separator_2);
		
		JLabel lblNewLabel_4 = new JLabel("Username");
		lblNewLabel_4.setBounds(24, 51, 69, 14);
		panel_3.add(lblNewLabel_4);
		
		textField_6 = new JTextField();
		textField_6.setBounds(138, 48, 143, 20);
		panel_3.add(textField_6);
		textField_6.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("Password");
		lblNewLabel_5.setBounds(24, 100, 69, 14);
		panel_3.add(lblNewLabel_5);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(138, 97, 143, 20);
		panel_3.add(passwordField);
		
		JButton btnSu = new JButton("View Orders");
		btnSu.addActionListener(new LoginListener());
		btnSu.setBounds(125, 198, 177, 63);
		panel_3.add(btnSu);
	}
	 public class LoginListener implements ActionListener{
			public void actionPerformed(ActionEvent event){
	            String username = "user";
	            char[] password = {'p','a','s','s'};
				String msg = null;
				String data = ord.readFile("Orders.txt");
				
				if(username.equals(textField_6.getText())){
	                if(Arrays.equals(passwordField.getPassword(), password)){
	                	msg = "Granted!";
	                	JOptionPane.showMessageDialog(null, msg);
	    	            JOptionPane.showMessageDialog(null, data);   
	                }                   
	           
	            }  
				else {
				    runnable.run();
	               	msg = "Denied!";
	                JOptionPane.showMessageDialog(null, msg);
	            }
	            textField_6.setText("");
	            passwordField.setText("");
	        }
	    }

	
	public static class Action implements ActionListener{

		
		public void actionPerformed(java.awt.event.ActionEvent evt) {
			// TODO Auto-generated method stub
			Float sum, getSum;
			
			Float price = Float.parseFloat(textField_7.getText());
			snack.setSnackPrice(price);
	
			int qty = Integer.parseInt(textField_1.getText());
			snack.setSnackQuantity(qty);
			
			String newLine = System.getProperty("line.separator");
			getSum = Float.parseFloat(textField_8.getText());	
			sum =  (snack.getSnackPrice() * snack.getSnackQuantity());
			
			getSum = getSum + sum;
			String sumString = Float.toString(sum);
			String totalString = Float.toString(getSum);
			textArea_1.setText(textArea_1.getText() + newLine + textField_1.getText() + " " + text + "    $" + sumString);
			textField_8.setText(totalString);
			
			textField_1.setText("");
			textField_5.setText(" ");
			textField_7.setText("");
			textField.setText("");
			lblNewLabel_8.setText("0");

		}
	}
}

