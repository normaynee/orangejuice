/*
 * Java bean class for entity "Order" 
 * Created on 2019-03-04 ( Date ISO 2019-03-04 - Time 04:38:50 )
 * Generated by Telosys Tools Generator ( version 3.0.0 )
 */

package org.demo.basic.bean;



/**
 * Java bean for entity "Order"
 * 
 * @author Keneil Ricketts and Michael Gayle //Telosys Tools Generator
 *
 */
public class Order // implements Serializable
{
    private String     orderId      ; // Id or Primary Key

    private String     location     ;
    private Float      totalPrice   ;
    private String     data         ;

    /**
     * Default constructor
     */
    public Order() {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR ID OR PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "orderId" field value
     * This field is mapped on the database column "orderId" ( type "", NotNull : true ) 
     * @param orderId
     */
	public void setOrderId( String orderId ) {
        this.orderId = orderId ;
    }
    /**
     * Get the "orderId" field value
     * This field is mapped on the database column "orderId" ( type "", NotNull : true ) 
     * @return the field value
     */
	public String getOrderId() {
        return this.orderId;
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR OTHER DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "location" field value
     * This field is mapped on the database column "location" ( type "", NotNull : true ) 
     * @param location
     */
    public void setLocation( String location ) {
        this.location = location;
    }
    /**
     * Get the "location" field value
     * This field is mapped on the database column "location" ( type "", NotNull : true ) 
     * @return the field value
     */
    public String getLocation() {
        return this.location;
    }

    /**
     * Set the "totalPrice" field value
     * This field is mapped on the database column "totalPrice" ( type "", NotNull : false ) 
     * @param totalPrice
     */
    public void setTotalPrice( Float totalPrice ) {
        this.totalPrice = totalPrice;
    }
    /**
     * Get the "totalPrice" field value
     * This field is mapped on the database column "totalPrice" ( type "", NotNull : false ) 
     * @return the field value
     */
    public Float getTotalPrice() {
        return this.totalPrice;
    }

    /**
     * Set the "data" field value
     * This field is mapped on the database column "data" ( type "", NotNull : true ) 
     * @param data
     */
    public void setData( String data ) {
        this.data = data;
    }
    /**
     * Get the "data" field value
     * This field is mapped on the database column "data" ( type "", NotNull : true ) 
     * @return the field value
     */
    public String getData() {
        return this.data;
    }

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    @Override
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append(orderId);
        sb.append("|");
        sb.append(location);
        sb.append("|");
        sb.append(totalPrice);
        sb.append("|");
        sb.append(data);
        return sb.toString(); 
    } 

}
