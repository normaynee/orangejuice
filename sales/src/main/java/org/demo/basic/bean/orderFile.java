package org.demo.basic.bean;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

public class orderFile {
	
	public void addStudent(String id, String fName, String lName, String location, String data){

		String fileName = "Orders.txt";
		
		try {
	        // Assume default encoding.
	        FileWriter fileWriter = new FileWriter(fileName, true);
	
	        
	        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
	

	       	bufferedWriter.write(id + " " + fName + " " + lName + " ");
	       	bufferedWriter.write(location);
	        bufferedWriter.write(data);
	        bufferedWriter.newLine();
	        bufferedWriter.newLine();
	        
	        bufferedWriter.close();
	    }
	    catch(IOException ex) {
	        System.out.println("Error writing to file '" + fileName + "'");

	    }
	}
	
}